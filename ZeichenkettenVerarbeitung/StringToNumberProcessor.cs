﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZeichenkettenVerarbeitung
{
    public static class StringToNumberProcessor
    {
        public static long ConvertInputToLong(string input)
        {
            long parsedLong = 0L;
            if (Int64.TryParse(input, out parsedLong))
            {
                return parsedLong;
            }
            else
            {
                return -1L;
            }
        }

        public static int QueersummeAusStringInputBerechnen(string numberInput)
        {
            int queersumme = 0;
            foreach(char sign in numberInput)
            {
                int signAsNumber = 0;
                Int32.TryParse(sign.ToString() , out signAsNumber);
                queersumme += signAsNumber;
            }
            return queersumme;
        }
    }
}
