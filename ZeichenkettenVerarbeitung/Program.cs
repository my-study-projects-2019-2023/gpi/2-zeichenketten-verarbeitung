﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZeichenkettenVerarbeitung
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bitte gebe eine Zahl ein:");
            String input = Console.ReadLine();
            if (StringToNumberProcessor.ConvertInputToLong(input) != -1L)
            {
                Console.Clear();
                Console.WriteLine("Deine Eingegebene Zahl:");
                Console.WriteLine(input);
                Console.WriteLine("Deine Eingegebene Zahl als Long:");
                Console.WriteLine(StringToNumberProcessor.ConvertInputToLong(input));
                Console.WriteLine("Die Queersumme deiner Eingegebenen Zahl:");
                Console.WriteLine(StringToNumberProcessor.QueersummeAusStringInputBerechnen(input));
            }
            else
            {
                Console.WriteLine("Die Eingabe stellt keine Zahl dar!");
            }
            Console.ReadKey();
        }
    }
}
