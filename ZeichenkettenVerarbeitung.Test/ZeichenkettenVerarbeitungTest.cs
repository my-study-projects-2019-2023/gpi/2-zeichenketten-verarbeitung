﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace ZeichenkettenVerarbeitung.Test
{
    [TestClass]
    public class ZeichenkettenVerarbeitungTest
    {
        [TestMethod]
        public void ParseStringToLongTest_withCorrectNumber()
        {
            String numberInput = "2537489579";
            long result = StringToNumberProcessor.ConvertInputToLong(numberInput);
            result.Should().Be(2537489579L);
        }

        [TestMethod]
        public void ParseStringToLongTest_withIncorrectNumber()
        {
            String numberInput = "2537489579b";
            long result = StringToNumberProcessor.ConvertInputToLong(numberInput);
            result.Should().Be(-1L);
        }

        [TestMethod]
        public void QueersummeBerechnenTest()
        {
            String numberInput = "123456";
            int result = StringToNumberProcessor.QueersummeAusStringInputBerechnen(numberInput);
            result.Should().Be(21);
        }
    }
}
